package com.concept.utilityClasses;

import java.util.Scanner;

import com.concept.polymorphism.DemoOverloading;

public class AuthenticationGateway {

	private static Scanner sc = new Scanner(System.in);
	
	public static boolean authenticate(String username,String password) {
		
		System.out.println("Enter username :");
		username = sc.next();
		System.out.println("Enter password :");
		password = sc.next();
		
		if (password.equals("shoaibali")) {
			System.out.println("Valid user");
			return true;
		} else {
			System.out.println("Invalid user");
			return false;
		}
	}
	
	public static void main(String[] args) { 
		
		AuthenticationGateway.authenticate("shoaib", "shoaibali");
	}

}
