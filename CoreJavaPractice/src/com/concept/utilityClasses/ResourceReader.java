package com.concept.utilityClasses;

import java.io.InputStream;
import java.util.Properties;

public class ResourceReader {

	private static Properties prop = null;

	public static Properties getProperties() {
		InputStream inputStream = ResourceReader.class.getClassLoader()
				.getResourceAsStream("resources//configuration.properties");
		if (prop == null)
			return readProperties(inputStream);
		else
			return prop;
	}

	private static Properties readProperties(InputStream inputStream) {
		try {
			prop = new Properties();
			// load a properties file
			prop.load(inputStream);

			inputStream.close();

			return prop;

		} catch (Exception ex) {
			prop = null;
			ex.printStackTrace();
			return null;
		}
	}
}
