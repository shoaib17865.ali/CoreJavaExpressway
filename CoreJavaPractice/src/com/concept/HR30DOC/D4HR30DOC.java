package com.concept.HR30DOC;

import java.io.*;
import java.util.*;

public class D4HR30DOC {
	private int age;

	public D4HR30DOC(int initialAge) {
		// Add some more code to run some checks on initialAge
		if (initialAge >= 0)
			age = initialAge;
		else {
			age = 0;
			System.out.println("Age is not valid, setting age to 0.");
		}
	}

	public void amIOld() {
		// Write code determining if this D4HR30DOC's age is old and print the correct
		// statement:
		if (age < 13)
			System.out.println("You are young.");
		else if (age >= 13 && age < 18)
			System.out.println("You are a teenager.");
		else
			System.out.println("You are old.");
	}

	public void yearPasses() {
		// Increment this D4HR30DOC's age.
		age += 1;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		for (int i = 0; i < T; i++) {
			int age = sc.nextInt();
			D4HR30DOC p = new D4HR30DOC(age);
			p.amIOld();
			for (int j = 0; j < 3; j++) {
				p.yearPasses();
			}
			p.amIOld();
			System.out.println();
		}
		sc.close();
	}
}
