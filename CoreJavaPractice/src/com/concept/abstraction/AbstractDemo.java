package com.concept.abstraction;

public class AbstractDemo extends AbstractClass1 implements InterfaceDemo2 {


	public AbstractDemo(int a) {
		super(a);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void numOfWheels() {
		// TODO Auto-generated method stub
		System.out.println("numOfWheels");
	}

	@Override
	public void sizeOfSteering() {
		// TODO Auto-generated method stub
		System.out.println("sizeOfSteering");
	}

	public static void main(String[] args) {
		AbstractDemo a = new AbstractDemo(1);
		a.numOfWheels();
		a.m1();
	}
}
