package com.concept.simpleFactoryDesignPattern;

import java.sql.Connection;
import java.sql.SQLException;

public class JDBCConnectionGenerator {

	public static Connection getConnection(ConnectDatabase dbName) throws SQLException {
		switch (dbName) {
		case MySQL_JDBC:
			return new MySQLDB().getJDBCConnection();
		case ORACLE_JDBC:
			return new Oracle().getJDBCConnection();
		case SyBASE_JDBC:
			return new Sybase().getJDBCConnection();
		default:
			throw new IllegalArgumentException("Database not found");
		}
	}
}
