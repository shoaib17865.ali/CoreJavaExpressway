package com.concept.simpleFactoryDesignPattern;

import java.sql.Connection;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class Sybase {

	public static Configuration configuration = new Configuration().configure("resources//test.db.xml");
	public static SessionFactory sessionFactory;
	public static Session session;
	
	private Connection con = null;
	
	public Session getConnection()
	{		
		if(sessionFactory == null)
			sessionFactory = configuration.buildSessionFactory();
		if(session!=null)
		{
			synchronized(session)
			{				
				session = sessionFactory.openSession();
				return session;
			}		
		}
		else
		{
			session = sessionFactory.openSession();
			return session;
		}		
	}
	
	public Connection getJDBCConnection()
	{
		return con;
	}
}
