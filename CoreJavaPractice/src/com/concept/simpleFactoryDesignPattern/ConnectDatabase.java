package com.concept.simpleFactoryDesignPattern;

public enum ConnectDatabase {
	
	MySQL, SyBASE, ORACLE,MySQL_JDBC, SyBASE_JDBC, ORACLE_JDBC;
}
