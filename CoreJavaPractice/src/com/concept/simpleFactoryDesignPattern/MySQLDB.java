package com.concept.simpleFactoryDesignPattern;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.concept.utilityClasses.EncryptDecrypt;

class MySQLDB {

	public static Configuration configuration = new Configuration().configure("resources//test.db.xml");
	public static SessionFactory sessionFactory;
	public static Session session;

	private Connection con = null;

	public Session getConnection() {
		if (sessionFactory == null)
			sessionFactory = configuration.buildSessionFactory();
		if (session != null) {
			synchronized (session) {
				session = sessionFactory.openSession();
				return session;
			}
		} else {
			session = sessionFactory.openSession();
			return session;
		}
	}

	public Connection getJDBCConnection() throws SQLException {
		String url = "jdbc:mysql://localhost:3306/jarvis"; // database specific url.
		String user = "root";
		String password = "yJWmcS5kxGs3Y6oaBc1gdw==";

		Connection con = DriverManager.getConnection(url, user, EncryptDecrypt.decrypt(password));
		return con;
	}
}
