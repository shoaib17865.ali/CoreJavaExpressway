/*
 * 
 */
package com.concept.simpleFactoryDesignPattern;

import java.sql.SQLException;

import com.concept.databaseOperation.HibernateCrudOperations;
import com.concept.databaseOperation.JDBCCrudOperations;

// TODO: Auto-generated Javadoc
/**
 * The Class TestDB.
 */
public class TestDB {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 * @throws SQLException
	 *             the SQL exception
	 */
	public static void main(String[] args) throws SQLException {
		HibernateCrudOperations testDB = new HibernateCrudOperations();
		JDBCCrudOperations operations = new JDBCCrudOperations();
		// testDB.getEmplpoyeeInformation();
		//operations.getEmplpoyeeInfoJDBC();
		//operations.getEmplpoyeeInfoJDBC2();
		operations.getEmplpoyeeInfoJDBC3();
		//operations.insertEmployeeData(1, "shoaib", "shoaib", "shoaib", "shoaib");
	}
}
