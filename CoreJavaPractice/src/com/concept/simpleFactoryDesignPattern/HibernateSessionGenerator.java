package com.concept.simpleFactoryDesignPattern;

import org.hibernate.Session;

public class HibernateSessionGenerator {

	public static Session getSession(ConnectDatabase dbName) {
		switch (dbName) {
		case MySQL:
			return new MySQLDB().getConnection();
		case ORACLE:
			return new Oracle().getConnection();
		case SyBASE:
			return new Sybase().getConnection();
		default:
			throw new IllegalArgumentException("Database not found");
		}
	}
}
