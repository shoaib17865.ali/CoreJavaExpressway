package com.concept.encapsulation;

public class BeanExp {

	private int id;
	private String name;
	private String empId;
	private String empOrg;
	private String place;
	
	public int getId() {
		return id;
	}
	private void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getEmpOrg() {
		return empOrg;
	}
	public void setEmpOrg(String empOrg) {
		this.empOrg = empOrg;
	}
	public String getPlace() {
		return place;
	}
	public void setPlace(String place) {
		this.place = place;
	}

}
