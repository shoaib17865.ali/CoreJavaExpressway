package com.concept.constructor;

public class DemoConstructor {

	static int a = 0;
	static int b = 0;
	
	static {
		System.out.println("static block.....");
		a = 90;
	}
	public DemoConstructor() {
		// TODO Auto-generated constructor stub
		System.out.println("Inside default constructor.....");
	}

	public DemoConstructor(int a, int b) {
		this.a=a;
		this.b=b;
		System.out.println("Inside parameterized constructor.....");
	}

	public static void main(String[] args) {
		
		Integer s= 20;
		Integer k= 20;
		String as = "shoaib";
		String ab = "shoaib";
		System.out.println();
	}
}
