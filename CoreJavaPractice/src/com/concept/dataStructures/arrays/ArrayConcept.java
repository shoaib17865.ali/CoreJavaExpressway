package com.concept.dataStructures.arrays;

public class ArrayConcept {

	public static void arrayOne() {
		int array[] = new int[20];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Element at index " + i + " : " + (array[i] = array.length - i));
		}
	}

	private static void arrayMultiDimensional() {
		// TODO Auto-generated method stub
		int arr[][] = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };

		// printing 2D array
		for (int i = 0; i < arr.length; i++) {
			for (int j =0; j < arr.length ; j++)
				System.out.print(arr[i][j] + " ");

			System.out.println();
		}
		System.out.println(arr.length);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		arrayOne();
		arrayMultiDimensional();
	}

}
