package com.concept.dataStructures.arrays;

public class LowArray {

	private long[] a; //creating a array
	
	public LowArray(int size) {
		//initilizing a array.
		a = new long[size];
	}
	
	public void setElement(int index, long value) {
		a[index] = value;
	}
	public long getElement(int index) {
		return a[index];
	}
}
