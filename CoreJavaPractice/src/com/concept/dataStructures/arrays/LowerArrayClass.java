package com.concept.dataStructures.arrays;

public class LowerArrayClass {

	public static void main(String[] args) {
		int size = 8;
		
		LowArray lwArr;
		lwArr = new LowArray(size);
		
		lwArr.setElement(0, 1);
		lwArr.setElement(1, 2);
		lwArr.setElement(2, 3);
		lwArr.setElement(3, 4);
		lwArr.setElement(4, 5);
		lwArr.setElement(5, 6);
		lwArr.setElement(6, 7);
		lwArr.setElement(7, 8);
		
		for (int i = 0; i < size; i++) {
			System.out.println(lwArr.getElement(i));
		}
	}
}
