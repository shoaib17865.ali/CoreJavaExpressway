package com.concept.databaseOperation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import com.concept.simpleFactoryDesignPattern.ConnectDatabase;
import com.concept.simpleFactoryDesignPattern.JDBCConnectionGenerator;

public class JDBCCrudOperations {

	Connection con = null;
	Statement stmt = null;
	PreparedStatement pstmt = null;

	/**
	 * Gets the emplpoyee info JDBC.
	 *
	 * @return the emplpoyee info JDBC
	 * @throws SQLException
	 *             the SQL exception
	 */
	public void getEmplpoyeeInfoJDBC() throws SQLException {

		con = JDBCConnectionGenerator.getConnection(ConnectDatabase.MySQL_JDBC);
		String hql = "select * from Employee";
		// Get a result set containing all data from test_table
		stmt = con.createStatement();
		ResultSet results = stmt.executeQuery(hql);
		// For each row of the result set ...
		while (results.next()) {

			System.out.println("Employee id : " + results.getInt(1));
			System.out.println("Employee name : " + results.getString(2));
			System.out.println("Employee address : " + results.getString(3));
			System.out.println("--------------------------------------------");
		}
		results.close();
		stmt.close();
		con.close();
	}
	
	public void getEmplpoyeeInfoJDBC2() throws SQLException {

		con = JDBCConnectionGenerator.getConnection(ConnectDatabase.MySQL_JDBC);
		String hql = "select employee_id, employee_name from Employee where id = 1";
		// Get a result set containing all data from test_table
		stmt = con.createStatement();
		ResultSet results = stmt.executeQuery(hql);
		// For each row of the result set ...
		while (results.next()) {
			System.out.println("------> Employee id : " + results.getString("employee_id")+", Employee name : " + results.getString("employee_name"));
			System.out.println("-----------------------------------------------------------------------------------------");
		}
		results.close();
		stmt.close();
		con.close();
	}
	
	public void getEmplpoyeeInfoJDBC3() throws SQLException {

		con = JDBCConnectionGenerator.getConnection(ConnectDatabase.MySQL_JDBC);
		ResultSet results = null;
		String hql = "select employee_id, employee_name from Employee where id = ?";
		// Get a result set containing all data from test_table
		pstmt = con.prepareStatement(hql);
		for (int i = 0; i <= 6 ; i++) {
			pstmt.setString(1,String.valueOf(i));
			results = pstmt.executeQuery();
			// For each row of the result set ...
			while (results.next()) {
				System.out.println("------> Employee id : " + results.getString("employee_id")+", Employee name : " + results.getString("employee_name"));
				System.out.println("-----------------------------------------------------------------------------------------");
			}
		}
		
		results.close();
		pstmt.close();
		con.close();
	}

	
	public void insertEmployeeData(int id, String eName, String eId, String eAddress, String eCol){

		try {
			con = JDBCConnectionGenerator.getConnection(ConnectDatabase.MySQL_JDBC);
			String hql = "INSERT INTO Employee VALUES (6, 'Saif Ali', 'GTS3036', 'Airoli7', 'iut')";
			// Get a result set containing all data from test_table
			// start transaction block
			con.setAutoCommit(false); // default true
			stmt = con.createStatement();
			stmt.executeUpdate(hql);

			// end transaction block, commit changes
			con.commit();
			System.out.println("Data Inserted successfully......");
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Error occurred during data insert......");
			e.printStackTrace();
			try {
				con.rollback();
				System.out.println("Rollback Executed...");
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				System.out.println("Rollback error...");
				e1.printStackTrace();
			}
		} finally {
			// TODO: handle finally clause
			try {
				//good practice to set it back to default true
				con.setAutoCommit(true);
				stmt.close();
				con.close();
				System.out.println("Finally executed successfully...");
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("Finnaly block error...");
				e.printStackTrace();
			}
			
		}
	}
}
