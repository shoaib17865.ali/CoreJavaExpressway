package com.concept.databaseOperation;

import java.util.HashMap;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import com.concept.pojo.Employee;
import com.concept.simpleFactoryDesignPattern.ConnectDatabase;
import com.concept.simpleFactoryDesignPattern.HibernateSessionGenerator;

public class HibernateCrudOperations {

	/**
	 * Gets the emplpoyee information.
	 *
	 * @return the emplpoyee information
	 */
	// Method to get fragment information with their fragment Id
	public void getEmplpoyeeInformation() {

		Session session = HibernateSessionGenerator.getSession(ConnectDatabase.MySQL);
		String hql = "from Employee";
		Query<?> query = session.createQuery(hql);
		@SuppressWarnings("unchecked")
		List<Employee> info = (List<Employee>) query.list();
		HashMap<String, String> pageInfo = new HashMap<String, String>();
		// converting the list result to HashMap
		for (Employee p : info) {
			System.out.println("Employee id : " + p.getEmployeeId());
			System.out.println("Employee name : " + p.getEmployeeName());
			System.out.println("Employee address : " + p.getEmployeeAddress());
			System.out.println("--------------------------------------------");
		}
		session.close();
	}
}
