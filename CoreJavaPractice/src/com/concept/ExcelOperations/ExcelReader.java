package com.concept.ExcelOperations;

import java.io.IOException;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;

// TODO: Auto-generated Javadoc
/**
 * The Class ExcelReader.
 */
public class ExcelReader {

	/** The Constant FILE_PATH. */
	private static final String FILE_PATH = "D:/DemoExcel.xlsx";
	private static final String SHEET_NAME = "Sheet1";
	
	/** The sheet. */
	private static Sheet sheet;
	
	/** The data formatter. */
	static DataFormatter dataFormatter = new DataFormatter();
	
	
	/**
	 * The main method.
	 *
	 * @param args the arguments
	 * @throws EncryptedDocumentException the encrypted document exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void main(String[] args) throws EncryptedDocumentException, IOException {

		/*System.out.println("This workbook has " + ReadingExcelFile.getInstance().getWorkbookInstance(FILE_PATH).getNumberOfSheets()
				+ " sheet with name : " + ReadingExcelFile.getInstance().getWorkbookInstance(FILE_PATH).getSheetName(0));
		ReadingExcelFile.getInstance().getInstance().getAllExcelData(FILE_PATH,SHEET_NAME);*/
        
        /*for (Row row : sheet) { // For each Row.
            Cell cell = row.getCell(0); // Get the Cell at the Index / Column you want.
            System.out.println(cell.getStringCellValue());
        }*/
		
		//getting all data in the excel.
		//ReadingExcelFile.getInstance().getAllExcelData(FILE_PATH, SHEET_NAME);
		boolean value = true;
		value = ReadingExcelFile.getInstance().clientValidateWithExcel("abc",FILE_PATH, SHEET_NAME);
		System.out.println(value);
        
        //file.getWorkbookInstance(FILE_PATH).close();
	}
}