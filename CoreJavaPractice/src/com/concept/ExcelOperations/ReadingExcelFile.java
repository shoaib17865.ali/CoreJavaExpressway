package com.concept.ExcelOperations;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.concept.designPattern.SimpleLazySingleton;

// TODO: Auto-generated Javadoc
/**
 * The Class ReadingExcelFile.
 */
@SuppressWarnings("serial")
public class ReadingExcelFile extends WorkbookFactory implements Cloneable,Serializable {

	/**
	 * Instantiates a new reading excel file.
	 */
	private ReadingExcelFile() {
	}
	
	/** The instance. */
	private static volatile ReadingExcelFile instance = null;
	
	/** The sheet. */
	private static Sheet sheet;
	
	/** The data formatter. */
	DataFormatter dataFormatter = new DataFormatter();

	/**
	 * Gets the workbook instance.
	 *
	 * @param fileName the file name
	 * @return the workbook instance
	 * @throws EncryptedDocumentException the encrypted document exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public Workbook getWorkbookInstance(String fileName) throws EncryptedDocumentException, IOException {
		// TODO Auto-generated constructor stub
		Workbook workbook = ReadingExcelFile.create(new File(fileName));
		return workbook;
	}
	
	public void getAllExcelData(String fileName, String sheetName) throws EncryptedDocumentException, IOException {

		sheet = ReadingExcelFile.getInstance().getWorkbookInstance(fileName).getSheet(sheetName);
		if (sheet != null) {
			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			// getting whole excel sheet
			for (Row row : sheet) {
				for (Cell cell : row) {
					String cellValue = dataFormatter.formatCellValue(cell);
					System.out.print(cellValue + "\t");
				}
				System.out.println();
			}
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		} else {
			System.out.println("No Sheet Found");
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		}
	}
	public ArrayList<String> getColumnData(String fileName, int cellNo, String sheetName) throws EncryptedDocumentException, IOException
	{
		ArrayList<String> columnData = new ArrayList<String>();
		sheet = ReadingExcelFile.getInstance().getWorkbookInstance(fileName).getSheet(sheetName);
		if (sheet != null) {
			System.out.println("\n\nIterating over Rows and Columns using for-each loop\n");
			for (Row row : sheet) {
				Cell cell = row.getCell(cellNo);
				columnData.add(cell.getStringCellValue());
			}
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
			return columnData;
		} else {
			System.out.println("No Sheet Found");
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		}
		return null;
	}

	// This code validates String with 2nd column data "Shoaib ali", "shoaib
	// khan".In above it will split and validate Shoaib in whole Column.
	public boolean clientValidateWithExcel(String clientName, String fileName, String sheetName)
			throws EncryptedDocumentException, IOException {
		sheet = ReadingExcelFile.getInstance().getWorkbookInstance(fileName).getSheet(sheetName);
		boolean client = true;
		if (sheet != null) {
			for (int rn = 1; rn < sheet.getLastRowNum(); rn++) {
				Row row = sheet.getRow(rn);
				String fundSheetName = row.getCell(0).getStringCellValue();
				//System.out.println(clientSheetName1);
				String[] clientSheetName = row.getCell(1).getStringCellValue().split(" ");
				String clientShName = clientSheetName[0];
				if (clientSheetName.length > 1) {
					String fundShName1 = clientSheetName[1];
					if (fundShName1.trim().equalsIgnoreCase(fundSheetName.trim()) == false) {
						client = false;
						System.out.println("Fund name wrong in row : "+ (rn+1));
						ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
					}
				} else {
					
					if (clientShName.trim().equalsIgnoreCase(clientName.trim()) == false) {
						client = false;
						System.out.println("Client name wrong in row : "+ (rn+1));
						ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
					}
					String fundShName = clientSheetName[1];
					if (fundShName.trim().equalsIgnoreCase(fundSheetName.trim()) == false) {
						client = false;
						System.out.println("Fund name wrong in row : " + (rn + 1));
						ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
					}
				}
				
				
			}
			if (client) {
				return true;
			}
			else {
				return false;
			}
		} else {
			System.out.println("No Sheet Found");
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		}
		ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		return true;
	}
	
	// getting all row data
	public void getRowData(String fileName, int rowNo, String sheetName)
			throws EncryptedDocumentException, IOException {

		sheet = ReadingExcelFile.getInstance().getWorkbookInstance(fileName).getSheet(sheetName);
		if (sheet != null) {
			for (int rn = 1; rn < sheet.getLastRowNum(); rn++) {
				Row row = sheet.getRow(rn);
				String a = row.getCell(0).getStringCellValue();
				String b = row.getCell(1).getStringCellValue();
				String c = row.getCell(2).getStringCellValue();
				String d = row.getCell(3).getStringCellValue();
				System.out.println("a :"+a+", b :"+b+", c :"+c+", d :"+d);
			}
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		} else {
			System.out.println("No Sheet Found");
			ReadingExcelFile.getInstance().getWorkbookInstance(fileName).close();
		}
	}
	
	/**
	 * Gets the single instance of ReadingExcelFile.
	 *
	 * @return single instance of ReadingExcelFile
	 */
	public static ReadingExcelFile getInstance() {
		if (instance == null) {
			instance = new ReadingExcelFile();
		}
		return instance;
	}

	public static ReadingExcelFile getSynchronizedInstance() {
		if (instance == null) {
			synchronized (ReadingExcelFile.class) {
				if (instance == null) {
					instance = new ReadingExcelFile();
				}
			}
		}
		return instance;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#clone()
	 */
	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		//return super.clone();
		return instance;
	}
	
	/**
	 * Read resolve.
	 *
	 * @return the object
	 */
	protected Object readResolve() 
    { 
        return instance; 
    } 
}
