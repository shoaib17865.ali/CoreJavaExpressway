package com.concept.polymorphism;

import java.util.Scanner;

public class DemoOverloading {

	static DemoOverloading overloading = new DemoOverloading();

	public void m1() {

		System.out.println("inside m1 method :");
	}

	public int m1(int a) {

		System.out.println("inside m2 method :");
		return a * a;
	}

	public int m1(int a, int b) {

		System.out.println("inside m3 method :");
		return a + b;
	}

	public double squareRoot(int num) {
		// temporary variable
		double t;
		double sqrtroot = num / 2;
		do {
			t = sqrtroot;
			sqrtroot = (t + (num / t)) / 2;
		} while ((t - sqrtroot) != 0);
		return sqrtroot;
	}

	public void calculateLogic(int a, int b, String operation) {

		switch (operation) {
		case "+": {
			System.out.println("Addition of 2 no is : " + overloading.m1(a, b));
			break;
		}
		case "sqr": {
			System.out.println("Square of a no is : " + overloading.m1(a));
			break;
		}
		case "sqrroot": {
			System.out.println("Square root of a no is : " + overloading.squareRoot(a));
			break;
		}
		default: {
			System.out.println("Entered operation is not valid");
			break;

		}
		}

	}

	public static void main(String[] args) {
		String operation;
		int a, b;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter operation :");
		operation = sc.next();
		System.out.println("Enter first no : ");
		a = sc.nextInt();
		if (operation.equals("sqr") || operation.equals("sqrroot") ) {
			b = 0;
			overloading.calculateLogic(a, b, operation);
		} else {
			System.out.println("Enter second no : ");
			b = sc.nextInt();
			overloading.calculateLogic(a, b, operation);
		}
	}
}
