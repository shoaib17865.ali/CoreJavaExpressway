package com.concept.polymorphism;

// TODO: Auto-generated Javadoc
/**
 * The Class TestOverLoad.
 */
public class TestOverLoad {

	/**
	 * M 1.
	 */
	public void m1() {
		// TODO Auto-generated method stub
		System.out.println("Inside m1() of 0 parameters");
	}

	public void m1(int a) {
		System.out.println("Inside m1() of int parameters");
	}
	
	public static void m1(Integer a) {
		System.out.println("Inside m1() of Integer parameters");
	}
	
	

	/**
	 * M 1.
	 *
	 * @param a the a
	 */
	public void m1(Object a) {
		System.out.println("Inside m1() of Object parameters");
	}
	public static void m2() {
		
	}

	public static void m2(int a) {

	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {

		TestOverLoad load = new TestOverLoad();
		// load.m1();
		load.m1(1);

	}
}
