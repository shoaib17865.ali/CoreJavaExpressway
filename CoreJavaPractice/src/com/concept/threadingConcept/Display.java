package com.concept.threadingConcept;

public class Display {

	
	public void wish(String name) {
		// TODO Auto-generated method stub
		for (int i = 0; i < 10; i++) {
			System.out.println("Good moring : ");
			try {
				Thread.sleep(800);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println(name);
		}
		synchronized (this) { //object level lock
			for (int i = 0; i < 10; i++) {
				System.out.println("Good night : ");
				try {
					Thread.sleep(800);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(name);
			}
		}
		synchronized (Display.class) { //class level lock
			for (int i = 0; i < 10; i++) {
				System.out.println("Good evening : ");
				try {
					Thread.sleep(800);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				System.out.println(name);
			}
		}
	}
}
