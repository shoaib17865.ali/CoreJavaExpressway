package com.concept.threadingConcept;

public class SynchronizedDemo {

	//static Display w;
	//https://www.youtube.com/watch?v=equLV2F0vLA
	public static void main(String[] args) {

		Display d = new Display();
		MyThread t1 = new MyThread(d, "shoaib");
		MyThread t2 = new MyThread(d, "azhar");
		
		//w.wish("");
		t1.start();
		t2.start();
	
	}
}
