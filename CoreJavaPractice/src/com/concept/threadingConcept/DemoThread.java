package com.concept.threadingConcept;

import java.util.Iterator;

public class DemoThread {

	
	private static void mi() {
		// TODO Auto-generated method stub
		int a = 10;
		for (int i = 0; i <= a; i++) {
			System.out.println("1st - Loop");
		}
		/*
		 * for (int i = 0; i <= a; i++) { System.out.println("2nd - Loop"); }
		 */
	}

	private static void extendingThreadExample() {
		// TODO Auto-generated method stub
		ExtendingThread t = new ExtendingThread();
		//t.setPriority(5);
		t.start(); // it will create a new thread
		t.interrupt();
		System.out.println("Priority of extendingThreadExample - "+t.getPriority());
		// t.start();

		/*
		 * 
		 * 1) register this thread with threadschedular & perform initializtion
		 * 	  activities
		 * 
		 * 2) calls run() method
		 * 
		 */
		//there are total 8 constructor in thread class	
		// t.run();

		//mi();
	}
	private static void implementingRunnableExample() {
		// TODO Auto-generated method stub
		ImplementingRunnable e = new ImplementingRunnable();
		Thread t1 = new Thread();
		Thread t2 = new Thread(e);
		t2.setPriority(1);
		t2.start();
		
	}
	
	private static void otherWayToDefineThread() {
		// TODO Auto-generated method stub
		ExtendingThread t = new ExtendingThread();
		Thread e = new Thread(t);
		e.start();

	}
	
	public static void gettingSettingThreadname() {
		// TODO Auto-generated method stub
		System.out.println(Thread.currentThread().getName());
		Thread.currentThread().setName("shoaib");
		System.out.println(Thread.currentThread().getName());
		//System.out.println("Current thread priority is - "+Thread.currentThread().MAX_PRIORITY);
		System.out.println("Priority of Current thread - "+Thread.currentThread().getPriority());
	}

	public static void main(String[] args) {
		
		// Thread.currentThread().setPriority(5); //setting main thread priority
		//implementingRunnableExample();
		extendingThreadExample();
		
		//gettingSettingThreadname();
		//mi();
		int a = 10;
		for (int i = 0; i <= a; i++) {
			
			System.out.println("1st - Loop");
		}
		
	}
}
