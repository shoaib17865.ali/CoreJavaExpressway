package com.concept.collectionPractice;

import java.util.ArrayList;
import java.util.List;

public class ListExample {

	public static void main(String[] args) {
		
		ListExample e = new ListExample();
		ArrayList a = new ArrayList();
		a.add(2);
		a.add(4);
		a.add(5);
		a.add(9);
		a.add(10);
		a.add(6);
		a.add(3);
		a.add(1);
		a.add(1);
		a.add(8);
		a.add(7);
		a.add(null);
		a.add(null);
		a.add(null);
		
		System.out.println(a);
		System.out.println(a.indexOf(6));
		System.out.println(a.remove(1));
		System.out.println(a);
		System.out.println(e);
	}
}
