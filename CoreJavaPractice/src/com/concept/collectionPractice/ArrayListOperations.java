package com.concept.collectionPractice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.Stack;

// TODO: Auto-generated Javadoc
/**
 * The Class ArrayListOperations.
 */
public class ArrayListOperations {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList list = new ArrayList<>();
		list.add(2);
		list.add(1);
		list.add(6);
		list.add(9);
		list.add(5);

		List l1 = Collections.synchronizedList(list);// making arraylist

		Stack s = new Stack();
		s.push(1);
		s.pop();
		s.push(3);
		s.pop();
		
		System.out.println(s);
		System.out.println(list);
		
	}

}
