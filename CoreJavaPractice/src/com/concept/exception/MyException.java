package com.concept.exception;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class MyException {

	static int a = 10;
	static int b = 0;

	public static void main(String[] args) {

		try {
			//System.exit(0); //finally will not be executed
			System.out.println("ascdascsa");
			System.out.println(a / b);
			//we can write finally wil try only when we get unchecked exception like ArithmeticException
			PrintWriter pw;
			pw = new PrintWriter("jtp.txt"); // may throw exception
			pw.println("saved");
			return ;

		} catch (FileNotFoundException e) {
			System.out.println("File not found exception");
			e.printStackTrace();
		} catch (ArithmeticException e) {
			System.out.println("ArithmeticException exception found");
			System.out.println(b / a);
			e.printStackTrace();
		} catch (Exception e) {
			System.out.println("Exception found");
			e.printStackTrace();
		}finally {
			System.out.println("finally executed");
		}
		System.out.println("one more code executed");
	}
}
