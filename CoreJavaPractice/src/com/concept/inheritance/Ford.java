package com.concept.inheritance;

public class Ford extends InheritExample {
 
	
	@Override
	public void steering() {
		// TODO Auto-generated method stub
		super.steering();
	}
	
	@Override
	public void steering(int a) {
		// TODO Auto-generated method stub
		super.steering(a);
	}
	
	public void colour() {
		// TODO Auto-generated method stub
		System.out.println("inside colour method");

	}
	public void seat() {
		// TODO Auto-generated method stub
		System.out.println("inside seat method");
	}
	public static void main(String[] args) {
		Ford ford = new Ford();
		ford.steering();
		ford.steering(1);
		ford.colour();
		ford.seat();
		
		Ford ford2 = (Ford) new InheritExample();
		
		InheritExample e =  new Ford();
		
		InheritExample e1 =  new InheritExample();
		
		
		
		
		
		
	}
}
