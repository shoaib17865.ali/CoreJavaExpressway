package com.concept.inheritance;

public class Toyota {


	static void overloadmethod(InheritExample a) {
		System.out.println("1");
	}

	static void overloadmethod(Nissan b) {
		System.out.println("2");
	}

	static void overloadmethod(Object obj) {
		System.out.println("3");
	}

	public static void main(String[] args) {
		InheritExample example = new Nissan();
		Ford ford = new Ford();
		Nissan nissan = new Nissan();
		String string = new String();
		overloadmethod(example);
	}
}
