package com.concept.stringConcept;

public class DemoHashCode_Equals {

	public static void main(String[] args) {
		
		CompanyEmployee e = new CompanyEmployee(1, "Shoaib");
		System.out.println(e.hashCode());
		CompanyEmployee e1 = new CompanyEmployee(1, "Ali");
		System.out.println(e1.hashCode());
		CompanyEmployee e3 = new CompanyEmployee(1, "Khan");
		System.out.println(e3.hashCode());
		
		String str1 = "FB";
		String str2 = "Ea";
		System.out.println(str1.hashCode()+" "+str2.hashCode());
		
		
		
	}
}
