package com.concept.stringConcept;

public class Student {

	String name;
	int rollNo;
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
		//return name+"   "+rollNo;
	}
	
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
	
	public Student(String name,int rollNo) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.rollNo = rollNo;
	}
	private void abc(String name,int rollNo) {
		// TODO Auto-generated method stub
		this.name = name;
		this.rollNo = rollNo;
	}
	public static void main(String[] args) {
		
		int ab;
		String k;
		
		Student sc = new Student("shoaib",1);
		Student sc1 = new Student("shoaib",1);
		System.out.println(sc);
		System.out.println(sc1);
		System.out.println(sc.hashCode());
		System.out.println(sc1.hashCode());
		System.out.println(Integer.toHexString(sc.hashCode()));
		System.out.println(Integer.toHexString(sc1.hashCode()));
		
		String a = new String("shoaib");
		String b = new String("azhar");
		String c = new String("shoaib");
		
		String d = "shoaib";
		String e = "shoaib";
		
		System.out.println(a.hashCode());
		System.out.println(b.hashCode());
		System.out.println(c.hashCode());
		System.out.println(a==c);
		System.out.println(a.equals(c));
		System.out.println(d==e);
		System.out.println(d.equals(e));
		System.out.println(d==a);
		System.out.println(d.equals(a));
		
		/*
		 * System.out.println(sc.name); System.out.println(sc.rollNo); sc.abc("azhar",
		 * 0); System.out.println(sc.name);
		 */
	}
}
